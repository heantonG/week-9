import numpy as np
import matplotlib.pyplot as plt
import cv2
import time
import pandas as pd

# boy = cv2.imread('hat.jpg')
# plt.imshow(boy, cmap='gray')

# boy = cv2.imread('hat.jpg', cv2.IMREAD_GRAYSCALE)
boy = cv2.imread('hat.jpg', cv2.COLOR_BGR2GRAY)
# plt.subplot(3,3,1)
# plt.imshow(cv2.cvtColor(boy, cv2.COLOR_BGR2RGB))
boy2 = cv2.imread('hat_highschool.jpg')
liu = cv2.imread('liu.jpg')
#unknow
liu_resize = cv2.resize(liu, boy2.shape[0:2][::-1])

cb = np.hstack((np.array(liu_resize), np.array(boy2))) 
plt.subplot(2,3,3)
plt.imshow(cv2.cvtColor(cb, cv2.COLOR_BGR2RGB))

#SIFT feature detector
sift = cv2.SIFT_create()
start_time = time.time()
#unknow
kp = sift.detect(boy, None)
print("elapsed time: %.6fs"%(time.time()-start_time) )
#Display the SIFT features
boy_sift = cv2.drawKeypoints(boy, kp, None)
plt.subplot(2,3,1)
plt.imshow(cv2.cvtColor(boy_sift, cv2.COLOR_BGR2RGB))

#Display the rich SIFT features
boy_sift2 = cv2.drawKeypoints(boy, kp, None, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
plt.subplot(2,3,2)
plt.imshow(cv2.cvtColor(boy_sift2, cv2.COLOR_BGR2RGB))

#SIFT FEATURE MATCHING
start_time2 = time.time()
kp1, des1 = sift.detectAndCompute(boy, None)
kp2, des2 = sift.detectAndCompute(cb, None)
print("elapsed time2: %.6fs" %(time.time() - start_time2))

print('image boy have -%d features' % des1.shape[0])
print('image cb have -%d features' % des2.shape[0])

#BFMatcher
bf = cv2.BFMatcher(cv2.NORM_L2)
matches = bf.knnMatch(des1, des2, k=2)
print('%d matches ' % len(matches))


#unknow
good_matches = []
for m,n in matches:
    if m.distance < 0.75 * n.distance:
        good_matches.append(m)
print('%d good matches' % len(good_matches))

#Display matches
corp = cv2.drawMatches(boy, kp1, cb, kp2, good_matches, None)
plt.subplot(2,3,4)
plt.imshow(cv2.cvtColor(corp, cv2.COLOR_BGR2RGB))


#SIFT MATCHING with Hellinger Distance
#L1 nomarlization
des1 = des1 / np.repeat(np.sum(des1, axis = 1).reshape(des1.shape[0], 1), des1.shape[1], axis=1)
des2 = des2 / np.repeat(np.sum(des2, axis = 1).reshape(des2.shape[0], 1), des2.shape[1], axis=1)
# calculate hellinger distance
dist_mat = np.sqrt(1.0 - np.dot(np.sqrt(des1), np.sqrt(des2).transpose()))

#matching with ratio test
min_arg = np.argsort(dist_mat, axis=1)
good_matches2 = []
for i in range(dist_mat.shape[0]):
    m, n  = min_arg[i][0:2]
    if dist_mat[i][m] < dist_mat[i][n] * 0.75:
        dmatch = cv2.DMatch(i, m, 0, dist_mat[i][m])
        good_matches2.append(dmatch)
print("%d helinger matches:" % len(good_matches2))

corp2 = cv2.drawMatches(boy, kp1, cb, kp2, good_matches2, None)
plt.subplot(2,3,5)
plt.imshow(cv2.cvtColor(corp2, cv2.COLOR_BGR2RGB))


plt.show()
