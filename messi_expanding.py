# from ast import Delete
from cProfile import label
import numpy as np
import cv2
import matplotlib.pyplot as plt

img = cv2.imread('messi.jpg', cv2.IMREAD_COLOR)
mh = img[70:150,190:280]
new_list = np.array([])
print("shape=", mh.shape)
print("mh=",mh)

plt.subplot(1,2,1)
plt.imshow(cv2.cvtColor(mh, cv2.COLOR_BGR2RGB))

for i in range(80):
    comt = mh[i,:]
    for j in range(89):
        ins = (comt[j+j] + comt[j+j+1])/2
        comt = np.insert(comt, j+j+1, values=ins, axis=0)
    new_list = np.append(new_list, comt.ravel())

ex_mh = new_list.reshape(80,179,3)
# print("ex_mh=",ex_mh)
# print("ex_mh.shape=",ex_mh.shape)
# print("dtype=",ex_mh.dtype)

ex_mh32 = ex_mh.astype(np.uint8)
# print("ex_mh32=",ex_mh32)
# print("ex_mh32,shape=", ex_mh32.shape)

# plt.subplot(3,1,2)
# plt.imshow(cv2.cvtColor(ex_mh32, cv2.COLOR_BGR2RGB))

ex_mh2 = ex_mh
for i in range(79):
    ins2 = (ex_mh[i] + ex_mh[i+1])/2
    ex_mh2 = np.insert(ex_mh2, i+1+i, values=ins2, axis=0)
# print("ex_mh2=",ex_mh2)
ex_mh2_32 = ex_mh2.astype(np.uint8)

plt.subplot(1,2,2)
plt.imshow(cv2.cvtColor(ex_mh2_32, cv2.COLOR_BGR2RGB), label="ugly messi")
plt.legend()

print("origin shape:",mh.shape, "ex shape:", ex_mh2_32.shape)
plt.show()
        
        
        
